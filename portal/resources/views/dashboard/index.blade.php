<!-- Dashboard Page-->

@extends('dashboard.layout')

@section('title')
Dashboard
@stop

@section('content')
 
 <div class="container-fluid">
	</br>
	</br>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Dashboard</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Dashboard
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
 

<!-- /.container-fluid -->
    
    
     <div class="row">
        
        <div class="col-sm-1">
        </div>
      
        <div class="col-sm-10">
          
          <div class="panel panel-info">
                  <!-- Default panel contents -->
                  <div class="panel-heading">{!! $user->firstName !!} {!! $user->lastName !!}</div>
  
            <div class="panel-body">
              <div class="row">                
                <div class=" col-md-12 col-lg-12 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        @foreach($branch as $userBranch)
                        <td><strong>Branch:</strong></td>
                        <td>{!! $userBranch->name !!}</td>
                        @endforeach
                      </tr>
                      <tr>
                        <td><strong>Hire Date:</strong></td>
                        <td>{!! $user->created_at->format('d M Y') !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Type:</strong></td>
                        <td>{!! $user->type !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Date of Birth:</strong></td>
                        <td>{!! $user->date_of_birth->format('d-M-Y') !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Last Active:</strong></td>
                        <td>{!! $user->lastActive->format('d M h:i a') !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Gender:</strong></td>
                        <td>{!! $user->gender !!}</td>
                      </tr>
                        <tr>
                        <td><strong>Home Address:</strong></td>
                        <td>{!! $user->address !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Email:</strong></td>
                        <td><a href="mailto:{!! $user->email !!}">{!! $user->email !!}</a></td>
                      </tr>
                      <tr>
                        <td><strong>Phone Number:</strong></td>
                        <td>{!! $user->homePhone !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Mobile:</strong></td>
                        <td>{!! $user->mobile !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Next Of Kin Name:</strong></td>
                        <td>{!! $user->nextOfKinName !!}</td>
                      </tr>
                      <tr>
                        <td><strong>Next Of Kin Contact:</strong></td>
                        <td>{!! $user->nextOfKinNumber !!}</td>
                      </tr>
                     
                    </tbody>
                  </table>
                  @if (Auth::user()->type === "Chaperone")
                  <a href="{{{ url("dashboard/chaperone/shift") }}}" class="btn btn-success">Start</a>
                  @endif
                  
                  @if (Auth::user()->type === "Staff")
                  <a href="{{{ url("dashboard/staff") }}}" class="btn btn-success">Start</a>
                  @endif
                 
                </div>
              </div>
            </div>
                
            
          </div>
        </div>
      </div>
 
 @stop
 