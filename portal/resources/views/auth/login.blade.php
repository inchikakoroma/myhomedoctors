<!DOCTYPE html>
<html class="full" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{{ asset('http://www.dahd.com.au/wp-content/uploads/2014/09/favicon.ico') }}}">

    <title>Login</title>

   
    <link rel="stylesheet" href="css/login.css" type="text/css" media="screen" />
     
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    
     <!-- Bootstrap core JavaScript
    ================================================== -->
  
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
     
  </head>

  <body>
   
     <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Buttom Nav Bar Links -->
                <a class="navbar-brand" href="{{ URL::to('./') }}"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                   
                    <li>
                        <a href="#"></a>
                    </li>
                   
                </ul>
            </div>
            <!-- end of navbar-collapse -->
        </div>
        <!-- end of container -->
    </nav>
    
    <!-- Page Content -->
    <div class="container">
        
        <img src="images/logo.png" class="img-responsive" alt="DAHD" class="logo">
 
            {!! Form::open(array('url' => 'auth/login', 'class' => 'form-signin')) !!}

        
            @if (count($errors) > 0)
             <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            
            </div>               
            @endif 
   
           {!! Form::email('email', null, array('required', 'class'=>'form-control', 'placeholder'=>'Email Address')) !!}
       
           {!! Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword', 'placeholder' => 'Password')) !!}
      
            <div class="checkbox">
            <label>
            <input type="checkbox" name="remember">  Remember me</label>
            </div>
            
         
      {!! Form::submit('Login', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
      {!! Form::close() !!}
       
    </div>
    <!--  end of container -->
    
    
    

  
  </body>
</html>