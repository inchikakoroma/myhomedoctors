@extends('auth.layout')

@section('title')
Create New User
@stop

 @section('content')
    
    <div class="row">
       
        <div class="col-sm-12">
    
              {!! Form::open(array('url' => ('user/new'))) !!}

              <div class="form-group">
                 {!! Form::label('firstName', 'First Name: ') !!}
                 <input type="text" class="form-control" name="firstName" value="{{ old('firstName') }}">
                 {!! $errors->first('firstName') !!}
              </div>
              
              <div class="form-group">
                 {!! Form::label('lastName', 'Last Name: ') !!}
                 <input type="text" class="form-control" name="lastName" value="{{ old('lastName') }}">
                 {!! $errors->first('lastName') !!}
              </div>
              
              
              <div class="form-group">
                
                <label>Gender</label>
                
                <label class="radio-inline">
                <input name="gender" id="gender" value="Male" checked="" type="radio">Male
                </label>
                                
                <label class="radio-inline">
                <input name="gender" id="gender" value="Female" checked="" type="radio">Female
                </label>
                
              </div>              
                            
              <div class="form-group">
                 {!! Form::label('address', 'Address: ') !!}
                 <textarea class="form-control" name="address" value="{{ old('address') }}"> </textarea>
                 {!! $errors->first('address') !!}
              </div>

              <div class="form-group">
                 {!! Form::label('email', 'Email: ') !!}
                 <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                 {!! $errors->first('email') !!}
              </div>
              
              <div class="form-group">
                 {!! Form::label('date_of_birth', 'Date Of Birth: ') !!}
                 <input type="text" id="datepicker" class="form-control" name="date_of_birth" value="{{ old('date_of_birth') }}">
                 {!! $errors->first('date_of_birth') !!}
              </div>
              
              <div class="form-group">
                {!! Form::label('branchID', 'Branch: ') !!}
                {!! Form::select('branchID', [
                    '4' => 'Armidale',
                    '9' => 'Auburn',
                   '7' => 'Brisbane',
                   '5' => 'Cessnock',
                   '8' => 'Gosford',
                   '3' => 'Mackay',
                   '6' => 'Newcastle',
                   '1' => 'Redcliffe',
                   '2' => 'Toowoomba']
                   
                ) !!}
              </div>
              
             
              <div class="form-group">
                {!! Form::label('password', 'Password: ') !!}
                {!! Form::password('password', array('class' => 'form-control')) !!}
                {!! $errors->first('password') !!}
              </div>

              <div class="form-group">
                {!! Form::label('password', 'Confirm Password: ') !!}
                {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                {!! $errors->first('password') !!}
              </div>

              <div class="form-group">
                {!! Form::label('type', 'Type: ') !!}
                {!! Form::select('type', [
                   'Staff' => 'Staff',
                   'Doctor' => 'Doctor',
                   'Chaperone' => 'Chaperone',
                    'Admin' => 'Admin']
                ) !!}
              </div>
              
              <div class="form-group">
                {!! Form::label('pass', 'Confirm: ') !!}
                {!! Form::password('pass', array('class' => 'form-control')) !!}
                @if($errors->any())
                <p style="color:red;">{{$errors->first()}}</p>
                @endif
                
              </div>
              
              
              {!! Form::submit('Create', array('class' => 'btn btn-success')) !!}
              <input type="reset" class="btn btn-info" value="Reset">
             
              {!! Form::close() !!}
              
        </div>
         
    </div>
 
 @stop