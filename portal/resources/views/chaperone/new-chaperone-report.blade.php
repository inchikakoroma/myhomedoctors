<!-- Dashboard Page-->


@extends('chaperone.layout')

@section('title')
Chaperone Report
@stop

@section('content')
 
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Chaperone Report</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-table"></i>  <a href="{{{ url("dashboard/chaperone") }}}">Chaperone</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> New Report
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
 

<!-- /.container-fluid -->
    
    
     <div class="row">
         
        
        <div id="overlay" style="display:none;">
           <img src="{{{ asset('images/loader.gif') }}}"  alt="Loading"><br/>
            Loading Please Wait...<br/>
            file uploads maybe slow
        </div>
        
        
        <div class="col-sm-12">
          
             {!! Form::open(array('url' => ('dashboard/chaperone/report/submit'), 'files'=>true)) !!}
              
            <div class="form-group">
                 <input type="hidden" name="userID" value="{!! $chaperoneShift->id !!}">
                 <p style="color:red;">{!! $errors->first('userID') !!}</p>
              </div>


            <div class="form-group">
                 {!! Form::label('patientName', 'Patient Name: ') !!}
                 <input type="text" class="form-control" name="patientName" value="{{ old('patientName') }}">
                 <p style="color:red;">{!! $errors->first('patientName') !!}</p>
                
            </div>
            
            <div class="form-group">
                 {!! Form::label('date_of_birth', 'Patient Date Of Birth: ') !!}
                 <input type="text" id="datepicker1" class="form-control" name="patientDOB" value="{{ old('patientDOB') }}">
                  <p style="color:red;">
                 {!! $errors->first('patientDOB') !!}
                 </p>
              </div>
            
             <div class="form-group">
                 {!! Form::label('Suburb', 'Surburb: ') !!}
                 <input type="text" class="form-control" name="surburb" value="{{ old('surburb') }}">
                <p style="color:red;">{!! $errors->first('surburb') !!}</p>
                
            </div>
            
            <div class="form-group">
                 {!! Form::label('startTime', 'Start Time: (16:00)') !!}
                 <input type="text" id="" data-time-format="H:i" class="form-control" name="startTime" value="{{ old('startTime') }}">
                  <p style="color:red;">{!! $errors->first('startTime') !!}</p>
                  
            </div>
              
               <div class="form-group">
                 {!! Form::label('finishTime', 'Finish Time: (23:00)') !!}
                 <input type="text" id="" data-time-format="H:i" class="form-control" name="finishTime" value="{{ old('finishTime') }}">
                  <p style="color:red;">{!! $errors->first('finishTime') !!}</p>
                 
              </div>
 
              <div class="form-group">
                {!! Form::label('medicareVoucher', 'Medicare Voucher: ') !!}
                {!! Form::file('medicareVoucher') !!}
                <p style="color:red;">{!! $errors->first('medicareVoucher') !!}</p>
               
              </div>
              
              {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default']) !!}
              {!! Form::submit('Save', array('class' => 'btn btn-success', 'id' => 'mySave')) !!}
              {!! Form::close() !!}
          
        </div>
        
     </div>
 
 @stop
 
 
 