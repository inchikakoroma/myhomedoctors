<!-- Dashboard Page-->
@extends('chaperone.layout')

@section('title')
Chaperone New Shift
@stop

@section('content')
 
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Chaperone Shift</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-table"></i>  <a href="{{{ url("dashboard/chaperone") }}}">Chaperone</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> New Shift
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
 

<!-- /.container-fluid -->
    
    
     <div class="row">
        
        
        <div class="col-sm-12">
            
            <div class="alert alert-danger">
              <strong>Alert!! </strong> Please provide all details
            </div>
          
         
                {!! Form::open(array('url' => ('dashboard/chaperone/shift/submit'))) !!}    
            
            </br>
              
               <div class="form-group">
                 {!! Form::label('firstName', "Chaperone's First Name: ") !!}
                 <input class="form-control" type="text" placeholder="{!! $user->firstName  !!}" readonly>
                <p style="color:red;">
                 {!! $errors->first('firstName') !!}
                 </p>
              </div>

            <div class="form-group">
                 {!! Form::label('lastName', "Chaperone's Last Name: ") !!}
                  <input class="form-control" type="text" placeholder="{!! $user->lastName  !!}" readonly>
                 <p style="color:red;">
                     {!! $errors->first('lastName') !!}
                 </p>
                 
            </div>
            
             <div class="form-group">
                 <input type="hidden" name="userID" value="{!! $user->id !!}">
                 <p style="color:red;">
                  {!! $errors->first('userID') !!}
                  </p>
              </div>
              
              <div class="form-group">
                 {!! Form::label('date', 'Date: ') !!}
                 <input type="text" id="datepicker1" class="form-control" name="date" value="{{ old('date') }}">
                 <p style="color:red;">
                 {!! $errors->first('date') !!}
                 </p>
              </div>
            
             <div class="form-group">
                 {!! Form::label('startTime', 'Start Time: (22:26)') !!}
                 <input type="text" id="timepicker" class="form-control" name="startTime" value="{{ old('startTime') }}">
                  <p style="color:red;">
                  {!! $errors->first('startTime') !!}
                  </p>
              </div>

              <div class="form-group">
                {!! Form::label('storeID', 'Location: ') !!}
                <select name="branchID">
                    @foreach($branch as $myBranch)
                    <option value="{{{$myBranch->id}}}">{{{$myBranch->name}}}</option>
                    @endforeach
                </select>
                <p style="color:red;">
                {!! $errors->first('branchID') !!}
                </p>
              </div>
             
            <label>{!! $br->name !!} Cars: </label>
            
             @foreach($car as $myCar)
            <label class="radio-inline">
               
                <input name="rego" id="rego"  value="{!!$myCar->rego!!}" checked="" type="radio">{{{$myCar->rego}}}
                
            </label>
            @endforeach
            <label class="radio-inline">
                 
                 <input name="rego" id="rego2"  value="other" type="radio">Other
            </label>
           
            
            </br>
            </br>
            
            <div class="form-group" id="otherRego">
                 {!! Form::label('otherRego', "Other Rego Number: (provide other rego number)") !!}
                 <input type="text" placeholder="" class="form-control" name="otherRego" value="{{ old('rego') }}">
                 <p style="color:red;">
                 {!! $errors->first('rego') !!}
                 </p>
            </div>
            
            <div class="form-group">
                 {!! Form::label('doctorsName', "Doctor's Name: ") !!}
                 <input type="text" class="form-control" name="doctorName" value="{{ old('doctorName') }}">
                 <p style="color:red;">
                 {!! $errors->first('doctorName') !!}
                  </p>
            </div>
            
            <div class="form-group">
                 {!! Form::label('startKilo', "Start Kilometres: ") !!}
                 <input type="text" class="form-control" placeholder="00000" name="startKilo" value="{{ old('startKilo') }}">
                 <p style="color:red;">
                 {!! $errors->first('startKilo') !!}
                 </p>
            </div>
            
             </br>
             

            
              {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default']) !!}
              {!! Form::submit('Next', array('class' => 'btn btn-success')) !!}
              {!! Form::close() !!}
          
        </div>
        
     </div>
 
 @stop
 