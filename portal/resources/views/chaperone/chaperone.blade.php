<!-- Dashboard Page-->


@extends('chaperone.layout')

@section('title')
Chaperone
@stop

@section('content')
 
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
        </br>
            <h1 class="page-header">
                <small>Chaperone</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-table"></i> Chaperone
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
 

<!-- /.container-fluid -->
    
    
    <div class="row">
           
               <a href="chaperone/shift" class="btn btn-success">New Shift</a>
              </br>
              </br>

            <div class="alert alert-danger">
              <strong>Alert!! </strong> You Must End Shift Before You Can Start A New Shift
            </div>
           
            </br>
            
            <div class="row">
        <div class="col-sm-12">
           
              @if (count($chaperoneShift) == 0)
             
              <p>No Shifts Found!! Click New Shift</p>
              
              
              @else 
             
              
    
               <div class="panel panel-default">
                  <!-- Default panel contents -->
                  <div class="panel-heading"><h5><strong>Shift Details</strong></h5></div>

                    <!-- Table -->
                    <table class="table">
                        
                    <thead> 
                        <tr> 
                            <th>ID</th> 
                            <th>Branch</th> 
                            <th>Start Time</th> 
                            <th>Car Rego</th>
                            <th>Doctor's Name</th>
                            <th>Status</th>
                        </tr> 
                    </thead> 
                     @foreach($chaperoneShift as $shift)
                    
                     @if ($shift->endKilo == null)
                    
                    
                    <tbody> 
                        <tr> 
                            
                            <td>{{{ $shift->id }}}</td> 
                            <td>{{{ $shift->branchName }}}</td>
                            <td>{{{ $shift->myDate }}} </td>
                            
                            @if($shift->rego == "other")
                               <td>{{{ $shift->otherRego }}}</td>
                              @else
                              <td>{{{ $shift->rego }}}</td>
                              @endif
 
                            <td>{{{ $shift->doctorName }}}</td>
                            <td>
                             @if ($shift->endKilo == null)
                              <a href="{!! route("dashboard-chaperone-shift", [$shift->id]) !!}" class="btn btn-success">Next</a>
                              @else
                              Close
                              @endif
                            </td>
                        </tr>
                    </tbody>
                    @endif
                    @endforeach

                    </table>
                    </div>
                      
                    
                
              
              @endif
          
        
      </div>
         
    
        </div>

        </div>
         
    </div>
 
 @stop
 