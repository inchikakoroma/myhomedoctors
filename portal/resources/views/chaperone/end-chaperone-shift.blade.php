<!-- Dashboard Page-->


@extends('chaperone.layout')

@section('title')
End Chaperone Shift
@stop

@section('content')
 
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>End Chaperone Shift</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-table"></i>  <a href="{{{ url("dashboard/chaperone") }}}">Chaperone</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i>Update Shift
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
 

<!-- /.container-fluid -->
    
    
     <div class="row">
      
        <div id="overlay" style="display:none;">
           <img src="{{{ asset('images/loader.gif') }}}" alt="Loading"><br/>
            Loading Please Wait...<br/>
            file uploads maybe slow
        </div>
        
        
        <div class="col-sm-12">
          
           {!! Form::model($chaperoneShift, array('method' => 'POST', 'files' => true,
                    'route' => array('update-shift', $chaperoneShift->id))) !!}
                    
              <h2>End Of Shift Questions</h2>
              </br>
              {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-info']) !!}
             </br>
             </br>
             
              <div class="form-group">
                 {!! Form::label('breakStartTime', 'Chaperone Break Start Time: (22:26)') !!}
                 <input type="text" id="timepicker" placeholder="" data-time-format="H:i" class="form-control" name="breakStartTime" value="{{ old('breakStartTime') }}">
                 <p style="color:red;">
                 {!! $errors->first('breakStartTime') !!}
                 </p>
              </div>
              
               <div class="form-group">
                 {!! Form::label('breakFinishTime', 'Chaperone Break Finish Time: (22:26)') !!}
                 <input type="text" id="timepicker2" placeholder="" data-time-format="H:i" class="form-control" name="breakFinishTime" value="{{ old('breakFinishTime') }}">
                 <p style="color:red;">
                 {!! $errors->first('breakFinishTime') !!}
                 </p>
              </div>
              
               <div class="form-group">
                 {!! Form::label('breakTotalHour', "Chaperone Break Total Hour: (1 Hour 30 mins)") !!}
                 <input type="text" class="form-control" placeholder="" name="breakTotalHour" value="{{ old('breakTotalHour') }}">
                <p style="color:red;">
                {!! $errors->first('breakTotalHour') !!}
                </p>
                </div>
                
                 <div class="form-group">
                 {!! Form::label('docStartTime', "Doctor's Start Time: (22:26)") !!}
                 <input type="text" id="timepicker3" placeholder="" data-time-format="H:i" class="form-control" name="docStartTime" value="{{ old('docStartTime') }}">
                 <p style="color:red;">
                 {!! $errors->first('docStartTime') !!}
                 </p>
              </div>
              
               <div class="form-group">
                 {!! Form::label('docFinishTime', "Doctor Finish Time: (22:26)") !!}
                 <input type="text" id="timepicker4" placeholder="" data-time-format="H:i" class="form-control" name="docFinishTime" value="{{ old('docFinishTime') }}">
                 <p style="color:red;">
                 {!! $errors->first('docFinishTime') !!}
                 </p>
              </div>
              
               <div class="form-group">
                 {!! Form::label('docTotalHour', "Doctor Total Hours: (1 Hour 30 mins)") !!}
                 <input type="text" class="form-control" placeholder="" name="docTotalHour" value="{{ old('docTotalHour') }}">
                <p style="color:red;">
                {!! $errors->first('docTotalHour') !!}
                 </p>
                </div>
                
                <div class="form-group">
                 {!! Form::label('', "Start Kilometres: ") !!}
                 <input id="startKilo" class="form-control" type="text"  value="{!!$chaperoneShift->startKilo!!}" placeholder="{!! $chaperoneShift->startKilo !!}" readonly>
                
               </div>
                
                 <div class="form-group">
                 {!! Form::label('endKilo', "End Kilometres: ") !!}
                 <input id="endKilo" type="text" class="form-control" placeholder="000000" name="endKilo" value="{{ old('endKilo') }}">
                <p style="color:red;">
                {!! $errors->first('endKilo') !!}
                 </p>
                </div>
                
          
            
                 <div class="form-group">
                 {!! Form::label('totalKilo', "Total Kilometres: ") !!}
                 <input id="totalKilo" type="text" class="form-control" placeholder="000" name="totalKilo" value="{{ old('totalKilo') }}">
                 <p style="color:red;">
                {!! $errors->first('endOfShift') !!}
                 </p>
                </div>
              
             
            <div class="form-group">
                
                <label>Vehicle Refueled</label>
                
                <label class="radio-inline">
                <input name="refueled" id="gender" value="Yes" checked="" type="radio">Yes
                </label>
                                
                <label class="radio-inline">
                <input name="refueled" id="gender" value="No" checked="" type="radio">No
                </label>
                </br>
                
                <label>Vehicle Refueled During Shift</label>
                
                <label class="radio-inline">
                <input name="refueledDuringShift" id="gender" value="Yes" checked="" type="radio">Yes
                </label>
                                
                <label class="radio-inline">
                <input name="refueledDuringShift" id="gender" value="No" checked="" type="radio">No
                </label>
                
                </br>
               
              </div> 
              
              <div class="form-group">
              {!! Form::label('reciept', 'Attached Receipt: ') !!}
                {!! Form::file('receipt') !!}
                <p style="color:red;">{!! $errors->first('reciept') !!}</p>
                </div>
                
                <div class="form-group">
                 {!! Form::label('finishTime', 'Finish Time: (07:26)') !!}
                 <input type="text" id="timepicker" data-time-format="H:i" class="form-control" name="finishTime" value="{{ old('finishTime') }}">
                  <p style="color:red;">
                  {!! $errors->first('finishTime') !!}
                  </p>
              </div>
              
              <div class="form-group">
                 {!! Form::label('petrolCost', "Petrol Cost: $10.50") !!}
                 <input type="text" class="form-control" placeholder="" name="petrolCost" value="{{ old('petrolCost') }}">
                <p style="color:red;">
                {!! $errors->first('petrolCost') !!}
                </p>
                </div>
                
                <label>Any Issues from Call Center</label>
                   
                <label class="radio-inline">
                <input name="feedBack" id="feedBack" checked="" value="No"  type="radio">No
                </label>
                
                <label class="radio-inline">
                <input name="feedBack" id="feedBack" value="Yes " type="radio">Yes
                </label>
                
                 </br>
                            
              <div class="form-group" id="feedBackText">
                 {!! Form::label('address', 'Feed Back: ') !!}
                 <textarea class="form-control" id="" name="feedBack" value="{{ old('feedBack') }}" placeholder="Job No: Issue: " rows="10"></textarea>
                 <p style="color:red;">
                 {!! $errors->first('feedBack') !!}
                 </p>
              </div>
              
             
              </br>
              

            
              {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default']) !!}
              {!! Form::submit('End', array('class' => 'btn btn-success', 'id' => 'mySave')) !!}
              {!! Form::close() !!}
          
        </div>
        
     </div>
 
 @stop
 