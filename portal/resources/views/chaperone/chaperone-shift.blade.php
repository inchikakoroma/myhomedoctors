<!-- Dashboard Page-->


@extends('chaperone.layout')

@section('title')
Chaperone Shift
@stop

@section('content')
 
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
        </br>
	
            <h1 class="page-header">
                <small>Chaperone Shift</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                 <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard/chaperone") }}}">Chaperone</a>
                </li>
                <li class="active">
                    <i class="fa fa-table"></i> Shift
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    
 
                     
      

<!-- /.container-fluid -->

        <div class="row">
			<div class="col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<span>Patients</span>
					</div>
					<div class="panel-body">
					    
					 <div class="alert alert-danger">
                     <strong>Info!! </strong> Please Scroll  
                     <span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>
                     (left) to EDIT and view more info on Smaller Devices
                    </div>
                          
                          <!-- Table -->
                <div class="table-responsive">
                    <table class="table">
                    <thead> 
                        <tr> 
                            <th>#</th>
                            <th>Patient Name</th> 
                            <th>Start Time</th> 
                            <th>Finish Time</th>
                            <th>Patient DOB</th>
                            <th>Surburb</th>
                            <th>Medicare Voucher</th>
                            <th>Consult Note</th>
                            <th></th>
                        </tr> 
                    </thead> 
                    
                    
                     
                    
                    <tbody> 
                    @foreach($chaperoneShift as $key=>$shift)
                    
                    <?php $key++; 
                     
                        $str = "https://myhomedoctors-inchikakoroma.c9users.io/portal/public";
                    
                        if($shift->voucher_url == NULL){
                        $string = "test/public/test";
                        $label = "No image";
                        $disable1 = "disabled";
                    	}else{
                    	$string = $shift->voucher_url;
                    	$label = "View";
                    	$disable1 = "";
                    	}
                    	if($shift->note_url == NULL){
                    	$string2 = "test/public/test";
                    	$label2 = "No image";
                    	$disable2 = "disabled";
                    	}else{
                        $string2 = $shift->note_url;
                        $label2 = "View";
                        $disable2 = "";
                    	}
                    
                    list($url,$querystring) = explode('/public', $string, 2);
                    list($url2, $querystring2) = explode('/public', $string2, 2);
                        
                        

                    ?>
                     
                         
                     
                     
                        <tr> 
                            <th scope="row">{{{ $key }}}</th> 
                            <td>{{{ $shift->patientName }}}</td> 
                            <td>{{{ $shift->start }}} </td>
                            <td>{{{ $shift->finish }}}</td>
                            <td>{{{ $shift->dob }}}</td>
                            <td>{{{ $shift->surburb }}}</td>
                            <td>
                               <a href="{{{$str}}}{{{$querystring}}}" data-lightbox="voucher" class="btn btn-info {{{$disable1}}}" data-title="Name:{{{$shift->patientName}}} - DOB:{{{ $shift->dob }}}">{{{$label}}}</a>
                            </td>
                            <td>
                            <a href="{{{$str}}}{{{$querystring2}}}" data-lightbox="notes" class="btn btn-info {{{$disable2}}}" data-title="Name:{{{$shift->patientName}}} - DOB:{{{ $shift->dob }}}">{{{$label2}}}</a>
                            </td>
                            <td>
                            <a href="{!! route("edit-report-view", [$shift->id]) !!}" class="btn btn-success">Edit</a>
                            </td>
                            
                        </tr>
                         @endforeach
                        
       
                    </tbody>
                    
                   

                    </table>
                </div>
                    <a href="{!! route("new-chaperone-report",  [$id]) !!}" class="btn btn-info">New</a>
                   
					</div>
				</div>
			</div>
		</div>

    
     <div class="row">
       
        <div class="col-sm-12">
            
            </br>
            
            <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">
                   Shift Details
                </div>
                <div class="panel-body">
            
              <div class="row">                
                <div class=" col-md-12 col-lg-12 "> 
                
                  <table class="table table-user-information">
                    <tbody>
                      @foreach($chaperoneShift1 as $shift)
                      
                       
                      
                      <tr>
                        <td><strong>Doctor's Name</strong></td>
                        <td>{{{$shift->doctorName }}}</td>
                      </tr>
                      
                      <tr>
                        <td><strong>Branch Name</strong></td>
                        <td>{!! $shift->branchName  !!}</td>
                      </tr>
                      
                      <tr>
                        <td><strong>Start Time</strong></td>
                        <td>{{{ $shift->myDate }}}</td>
                      </tr>
                     
                        
                    </tbody>
                    
                     @endforeach
                  </table>
                   <a href="{!! route("endShift", [$shift->currentID]) !!}" class="btn btn-primary">End Shift</a>
                 
                </div>
              </div>
            </div>
                
                </div>
            </div>
    
        </div>
</div>
 
 @stop
 