<!-- Dashboard Page-->

@extends('chaperone.layout')

@section('title')
Edit Chaperone Report
@stop

@section('content')
 
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Edit Chaperone Report</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-table"></i>  <a href="{{{ url("dashboard/chaperone") }}}">Chaperone</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Edit Report
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
 

<!-- /.container-fluid -->
    
    
     <div class="row">
         
         <div id="overlay" style="display:none;">
           <img src="{{{ asset('images/loader.gif') }}}" alt="Loading"><br/>
            Loading Please Wait...
        </div>
        
        
        <div class="col-sm-12">
          
           {!! Form::model($chaperoneReport, array('method' => 'POST', 'files'=>true,
                    'route' => array('edit-report', $chaperoneReport->id))) !!}
              
            <div class="form-group">
                 {!! Form::label('patientName', 'Patient Name: ') !!}
                 <input type="text" class="form-control" name="patientName" value="{!! $chaperoneReport->patientName !!}">
                 {!! $errors->first('patientName') !!}
            </div>
            
            <div class="form-group">
                 {!! Form::label('date_of_birth', 'Patient Date Of Birth: ') !!}
                 <input type="text" id="datepicker1" class="form-control" name="patientDOB" value="{!! $chaperoneReport->patientDOB !!}">
                 {!! $errors->first('patientDOB') !!}
              </div>
            
             <div class="form-group">
                 {!! Form::label('Suburb', 'Surburb: ') !!}
                 <input type="text" class="form-control" name="surburb" placeholder="North Sydney" value="{!! $chaperoneReport->surburb !!}">
                 {!! $errors->first('surburb') !!}
            </div>
            
            <div class="form-group">
                 {!! Form::label('startTime', 'Start Time: (16:00) ') !!}
                 <input type="text" id="timepicker" placeholder="19:20" data-time-format="H:i" class="form-control" name="startTime" value="{!! $chaperoneReport->startTime !!}">
                 {!! $errors->first('startTime') !!}
              </div>
              
               <div class="form-group">
                 {!! Form::label('finishTime', 'Finish Time: (23:00) ') !!}
                 <input type="text" id="timepicker" placeholder="20:20" class="form-control" name="finishTime" value="{!! $chaperoneReport->finishTime !!}">
                 {!! $errors->first('finishTime') !!}
              </div>
              
               <div class="form-group">
                {!! Form::label('medicareVoucher', 'Update Medicare Voucher: ') !!}
                {!! Form::file('medicareVoucher') !!}
                {!! $errors->first('medicareVoucher') !!}
              </div>
              
              <div class="form-group">
                 {!! Form::label('consultNote', 'Update Consult Note: ') !!}
                {!! Form::file('consultNote') !!}
                {!! $errors->first('consultNote') !!}
              </div>
              
              {!! link_to(URL::previous(), 'Cancel', ['class' => 'btn btn-default']) !!}
              {!! Form::submit('Save', array('class' => 'btn btn-success', 'id' => 'mySave')) !!}
              {!! Form::close() !!}
          
        </div>
        
     </div>
 
 @stop
 