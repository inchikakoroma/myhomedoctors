@extends('staff.layout')

@section('title')
{{{$name}}} Chaperone Shifts 
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>{{{$name}}} Chaperone Shifts</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-list"></i>  <a href="{{{ url("dashboard/staff") }}}">Staff</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i>Chaperone Branch Shifts
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
</div> 

<!-- /.container-fluid -->
    
        <div class="row">
         
        <div class="col-sm-12">
            
            @if (count($chaperoneShift) == 0)
             
              <p>No Shifts found for {{{$name}}}</p>
             
             @else 
            
            <div class="panel panel-success">
                  <!-- Default panel contents -->
                  <div class="panel-heading"><h5><strong>Shift Details</strong></h5></div>

                    <!-- Table -->
                    <table class="table">
                        
                    <thead> 
                        <tr> 
                            <th>#</th>
                            <th>Date</th>
                            <th>Chaperone's Name</th>
                            <th>Branch</th> 
                            <th>Start Time</th> 
                            <th>Finish Time</th>
                            <th>Doctor's Name</th>
                            <th>Doctor's Start Time</th>
                            <th>Doctor's Finish Time</th>
                            <th></th>
                        </tr> 
                    </thead> 
                     @foreach($chaperoneShift as  $key=>$shift)
                  <?php $key++;?>
                    <tbody> 
                        <tr> 
                            
                            <th scope="row">{{{ $key }}}</th>
                            <td>{{{ $shift->date }}}</td>
                            <td>{{{ $shift->firstName}}} {{{$shift->lastName}}}</td> 
                            <td>{{{ $shift->branchName }}}</td>
                            <td>{{{ $shift->chapStart }}} </td>
                            <td>{{{ $shift->chapFinish }}}</td>
                            <td>{{{ $shift->doctorName }}}</td>
                            <td>{{{ $shift->docStart }}}</td>
                            <td>{{{ $shift->docFinish }}}</td>
                            <td>
                              <a href="{!! route("staff-chaperone-shifts-details", [$shift->currentID]) !!}" class="btn btn-success">View Report</a>
                            </td>
                        </tr>
                    </tbody>
                   
                    @endforeach

                    </table>
                    </div>
            
            @endif
             
        </div>
        
    
     </div>
    
 
 @stop
 