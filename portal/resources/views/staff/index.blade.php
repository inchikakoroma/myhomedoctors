<!-- Dashboard Page-->


@extends('dashboard.layout')

@section('title')
Dashboard
@stop

@section('content')
 
 <div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Dashboard</small>
            </h1>
            
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-list"></i> Staff
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
 

<!-- /.container-fluid -->
    
    
    <div class="row">
        
        <div class="col-sm-2">
            
        </div>
        
        <div class="col-sm-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-bookmark"></span> Quick Shortcuts</h3>
                </div>
                <div class="panel-body">
                    
                    <div class="row">
                        
                        </br>
                       
                        <div class="col-md-3">
                             <a href="#" class="btn btn-info btn-xlarge" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>Chaperone</br> Shifts</a>
                        </div>
                       
                      
                        
                        <div class="col-md-3">
                            <a href="{{{ url("dashboard/staff/chaperone-shifts") }}}" class="btn btn-info btn-xlarge" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>All Chaperone</br> Shifts</a>
                        </div>
                        
                        <div class="col-md-3">
                            <a href="#" class="btn btn-info btn-xlarge" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>Chaperone</br> Report</a>
                        </div>
                        
                        <div class="col-md-3">
                            <a href="{{{ url("dashboard/staff/chaperone-reports") }}}" class="btn btn-info btn-xlarge" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>All Chaperone</br> Report</a>
                        </div>
                        
                        
                    </div>
                     
                    <div class="row">
                        
                        <div class="col-md-3">
                            
                            <div class="dropdown">
                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Branches
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                 @foreach($branch as $myBranch)
                                  <li><a href="{!! route("staff-chaperone-branch-shifts",[$myBranch->id, $myBranch->name] ) !!}">{{{$myBranch->name}}}</a></li>
                                  @endforeach 
                              </ul>
                            </div>
                            
                        </div>
                        
                        <div class="col-md-3">
                            
                        </div>
                        
                        <div class="col-md-3">
                            
                            <div class="dropdown">
                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Branches
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                 @foreach($branch as $myBranch)
                                  <li><a href="{!! route("staff-chaperone-branch-reports",[$myBranch->id, $myBranch->name]) !!}">{{{$myBranch->name}}}</a></li>
                                  @endforeach 
                              </ul>
                            </div>
                            
                        </div>
                        
                        <div class="col-md-3">
                            
                        </div>
                        
                    </div>
                    
                    </br>
                    
                    <div class="row">
                        
                        
                       
                        <div class="col-md-3">
                             <a href="{!! route("staff-consult-notes") !!}" class="btn btn-info btn-xlarge" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>All Consult</br> Notes</a>
                        </div>
                        
                        <div class="col-md-3">
                            <a href="#" class="btn btn-info btn-xlarge disabled" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>Head Office</br> Shifts</a>
                        </div>
                        
                        <div class="col-md-3">
                            <a href="#" class="btn btn-info btn-xlarge disabled" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>All Doctor's</br> Shifts</a>
                        </div>
                        
                        <div class="col-md-3">
                             <a href="#" class="btn btn-info btn-xlarge disabled" role="button">
                                 <span class="glyphicon glyphicon-user"></span> 
                                 <br/>Call Centre</br> Shifts</a>
                        </div>
                        
                      
                    </div>
                   
                   
                </div>
                </br>
                </br>
            </div>
        </div>
        
        <div class="col-sm-2">
            
        </div>
        
    </div>

 @stop