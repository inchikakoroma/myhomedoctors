@extends('staff.layout')

@section('title')
Chaperone Shift Details
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>Chaperone Report</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-list"></i>  <a href="{{{ url("dashboard/staff") }}}">Staff</a>
                </li>
                <li class="active">
                   <i class="fa fa-table"></i>  <a href="{{{ url("dashboard/staff/chaperone-shifts") }}}">Shifts</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Shift Details
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
</div> 

<!-- /.container-fluid -->
    
    
<!-- Shift Details -->

@if (count($chaperoneShift1) == 0)

<p>No Chaperone Shift</p>
              
              
@else 

    
@foreach($chaperoneShift1 as $chaperoneShift)


<div class="row">
        
        
        <div class="col-sm-12">
          
          <div class="panel panel-success">
                  <!-- Default panel contents -->
                  <div class="panel-heading"><h3>{{{$chaperoneShift->firstName}}} {{{$chaperoneShift->lastName}}}</h3></div>
  
            <div class="panel-body">
                
                <div class="row">
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Shift ID</span>
                            <input type="text" value="{!! $chaperoneShift->currentID !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        
                            <span class="input-group-addon info" id="basic-addon3">Date</span>
                            <input type="text"value="{!! $chaperoneShift->date !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                       
                    </div>
                    
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Start Time</span>
                            <input type="text" value="{!! $chaperoneShift->chapStart !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Finish Time</span>
                            <input type="text" value="{!! $chaperoneShift->chapFinish !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Branch</span>
                            <input type="text" value="{!! $chaperoneShift->branchName !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                 
                </div>
                
                </br>
                
                <div class="row">
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Car Rego</span>
                            <input type="text" value="{!! $chaperoneShift->rego !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Start Kilometers</span>
                            <input type="text" value="{!! $chaperoneShift->startKilo !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">End Kilometers</span>
                            <input type="text" value="{!! $chaperoneShift->endKilo !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Total Kilometers</span>
                            <input type="text" value="{!! $chaperoneShift->totalKilo !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                 
                </div>
                
                </br>
                
                <div class="row">
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Break Start Time</span>
                            <input type="text" value="{!! $chaperoneShift->breakStart !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Break Finish Time</span>
                            <input type="text" value="{!! $chaperoneShift->breakFinish !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Doctor's Start Time</span>
                            <input type="text" value="{!! $chaperoneShift->docStart !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Doctor's Finish Time</span>
                            <input type="text" value="{!! $chaperoneShift->docFinish !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                 
                </div>
                
                </br>
                
                 <?php
                    
                    $str = "https://myhomedoctors-inchikakoroma.c9users.io/portal/public";
                    
                    if($chaperoneShift->receipt_url == null){
                        $string = "/public/";
                        $label = "No Receipt found!!!";
                        $disable = "disabled";
                    }else{
                        $string =  $chaperoneShift->receipt_url;
                        $label = "";
                        $disable = "";
                    }
                    
                    list($url,$querystring) = explode('/public', $string, 2);

                     ?>

                <div class="row">
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Fueled</span>
                            <input type="text" value="{!! $chaperoneShift->refueled !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Fueled During Shift</span>
                            <input type="text" value="{!! $chaperoneShift->refueledDuringShift !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Petrol Cost</span>
                            <input type="text" value="{!! $chaperoneShift->cost !!}" class="form-control" id="basic-url" aria-describedby="basic-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                       <a href="{{{$str}}}{{{$querystring}}}" data-lightbox="myImages" class="btn btn-info {{{$disable}}}" data-title="{{{$chaperoneShift->cost}}}">View Receipt</a>
                       {{{$label}}}
                    </div>
                 
                </div>
                
                </br>
                
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon info" id="basic-addon3">Doctor's Name</span>
                            <input type="text" value="{!! $chaperoneShift->docName !!}" id="basic-url" class="form-control" aria-describedby="sizing-addon3" readonly>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon info" id="sizing-addon3">Feed Back</span>
                            <input type="text" value="{!! $chaperoneShift->feedback !!}" id="basic-url" class="form-control" aria-describedby="sizing-addon3" readonly>
                        </div>
                    </div>

                </div>
                
            
          </div>
        </div>
      </div>
</div>

@endforeach
@endif

    <div class="row">
			<div class="col-sm-12">
			   
			   
                    
                     @if (count($chaperoneReport) == 0)
             
                      <p>No Chaperone Report</p>
              
              
                      @else 
		
				<div class="panel panel-success">
					<div class="panel-heading">
						<span><h3>Patients</h3></span>
					</div>
					<div class="panel-body">
                          
                          <!-- Table -->
                    <table class="table">
                    <thead> 
                        <tr> 
                            <th>#</th>
                            <th>Date</th>
                            <th>Patient's Name</th> 
                            <th>Start Time</th> 
                            <th>Finish Time</th>
                            <th>Patient DOB</th>
                            <th>Billing Code</th>
                            <th>Surburb</th>
                            <th>Medicare Voucher</th>
                            <th>Consult Note</th>
                            <th></th>
                        </tr> 
                    </thead> 
                
                    @foreach($chaperoneReport as $key=>$shift)
                    <tbody> 
                 
                     <?php $key++; 
                     
                     $str = "https://myhomedoctors-inchikakoroma.c9users.io/portal/public";
                     
                    
                    

                    if($shift->voucher_url == null){
                        $string = "/public/";
                        $label = "No image";
                        $disable1 = "disabled";
                    }else{
                        $string = $shift->voucher_url;                      
                        $label = "Voucher";
                        $disable1 = "";
                    }
                    
                    if($shift->note_url == null){                        
                        $string2 = "/public/";
                        $label2 = "No image";
                        $disable2 = "disabled";
                    }else{                        
                        $string2 = $shift->note_url;
                        $label2 = "Consult Note";
                        $disable2 = "";
                    }
                    
                    list($url,$querystring) = explode('/public', $string, 2);
                    list($url2, $querystring2) = explode('/public', $string2, 2);
                    
                     ?>
                     
                     
                        <tr> 
                            <th scope="row">{{{ $key }}}</th>
                            <td>{{{ $shift->date }}}</td> 
                            <td>{{{ $shift->patientName }}}</td> 
                            <td>{{{ $shift->reportStart }}} </td>
                            <td>{{{ $shift->reportFinish }}}</td>
                            <td>{{{ $shift->dob }}}</td>
                            <td>456</td>
                            <td>{{{ $shift->surburb }}}</td>
                            <td>
                             <a href="{{{$str}}}{{{$querystring}}}" data-lightbox="voucher" class="btn btn-info {{{$disable1}}}" data-title="Name:{{{$shift->patientName}}} - DOB:{{{ $shift->dob }}}">{{{$label}}}</a>
                            </td>
                            <td>
                             <a href="{{{$str}}}{{{$querystring2}}}" data-lightbox="notes" class="btn btn-info {{{$disable2}}}" data-title="Name:{{{$shift->patientName}}} - DOB:{{{ $shift->dob }}}">{{{$label2}}}</a>
                            </td>
                            
                        </tr>
               
                   
                       
                        
       
                    </tbody>
                     @endforeach
                   

                    </table>
                 
					</div>
				</div>
			</div>
			
			@endif
                        
		</div>
 
 @stop
 