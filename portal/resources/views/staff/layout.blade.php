<!DOCTYPE html>
<!-- List item. -->
<html>

 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <title>@yield('title')</title>
   
    <!-- Icon -->
    <link rel="icon" href="{{{ asset('http://www.dahd.com.au/wp-content/uploads/2014/09/favicon.ico') }}}">
    
    <!-- Custom Fonts -->
    <link rel="stylesheet" href="{{{ asset('font-awesome/css/font-awesome.min.css') }}}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{{ asset('css/sb-admin.css') }}}">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{{ asset('css/login.css') }}}">
    
    <!-- Lightbox CSS -->
    <link rel="stylesheet" href="{{{ asset('lightbox2/dist/css/lightbox.css') }}}">
    
    <!-- jQuery CSS -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{{ asset('css/bootstrap.min.css') }}}">

  </head>

<body>
  
  <div id="wrapper">  
     
     
     <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{{ url("dashboard") }}}">DAHD Staff Panel</a>
            </div>
            @if (Auth::check())
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
            <!-- Message Preview -->
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading">
                                            <strong>
                                              {!! Auth::user()->firstName !!}
                                              {!! Auth::user()->lastName !!}
                                            </strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                       
                       
                    </ul>
                </li>
                
          <!-- Alerts -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default"></span></a>
                        </li>
                        
                    </ul>
                </li>
                
          <!-- User -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-user"></i> 
                      {!! Auth::user()->firstName !!}
                      {!! Auth::user()->lastName !!}
                      <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                        <!-- User  <a href="{!! route("profile", [Auth::user()->id]) !!}"><i class="fa fa-fw fa-user"></i> Profile</a> -->
                         <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-sign-out"></i>
                            @if (Auth::check())
                            {!! Auth::user()->active->format('d M h:i a') !!}
                            @endif
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{{ url("auth/logout") }}}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                
              @endif
               
            </ul>
            
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
          
           <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                     @if (Auth::check())
                    <li>
                        <a href="{{{ url("dashboard") }}}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    @if (Auth::user()->type === "Chaperone" || Auth::user()->type === "Admin")
                    <li>
                        <a href="dashboard/chaperone"><i class="fa fa-fw fa-table"></i>Chaperone</a>
                    </li>
                     
                    @endif
                    @if (Auth::user()->type === "Staff" || Auth::user()->type === "Admin")
                    <li>
                        <a href="{{{ url("dashboard/staff") }}}"><i class="fa fa-fw fa-desktop"></i>Staff</a>
                    </li>
                    @endif
                   
                    
                    @if (Auth::user()->type === "Admin")
                    <li>
                        <a href="{{{ url("admin-users") }}}"><i class="fa fa-fw fa-wrench"></i>Show Users</a>
                    </li>
                    @endif
                     @if (Auth::user()->type === "Manager" || Auth::user()->type === "Admin")
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i>Manage Staff<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Create Rooster</a>
                            </li>
                            
                            <li>
                                <a href="{{{ url("#") }}}">Staff Details</a>
                            </li>
                        </ul>
                    </li>
                   
                    @endif
            @else
                    <li>
                        <a href="{{{ url("./") }}}"><i class="fa fa-fw fa-dashboard"></i>Home</a>
                    </li>
                <li>
                  <li><a href="{{ URL::to("auth/register") }}"><i class="fa fa-fw fa-edit"></i>Sign Up</a></li>
                </li>
              @endif
                   
                </ul>
            </div>
           
           
           
           
            <!-- /.navbar-collapse -->
        </nav>
        
        <div id="page-wrapper">

            @section('content')
            @show

        </div>
        <!-- /#page-wrapper -->

      
    
  </div> <!-- /container -->
  
   <!-- JavaScript
    ================================================== -->

      
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
      <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      <script src="{{{ asset('lightbox2/dist/js/lightbox-plus-jquery.min.js') }}}"></script>
      
      
       
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.11/jquery.timepicker.min.js"></script>
      <script src="{{{ asset('js/script.js') }}}"></script>
   
</body>
</html>