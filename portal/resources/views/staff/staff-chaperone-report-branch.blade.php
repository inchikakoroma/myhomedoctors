@extends('staff.layout')

@section('title')
{{{$name}}} Chaperone Reports
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>{{{$name}}} Chaperone Reports</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-list"></i>  <a href="{{{ url("dashboard/staff") }}}">Staff</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i>Chaperone Branch Reports
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
</div> 

<!-- /.container-fluid -->
    
    <div class="row">
			<div class="col-sm-12">
			   
			   
                    
                     @if (count($chaperoneReport) == 0)
             
                      <p>No report found for {{{$name}}}</p>
              
              
                      @else 
		
				<div class="panel panel-success">
					<div class="panel-heading">
						<span><h5>Reports Details</h5></span>
					</div>
					<div class="panel-body">
                          
                          <!-- Table -->
                    <table class="table">
                    <thead> 
                        <tr> 
                            <th>#</th>
                            <th>Date</th>
                            <th>Patient's Name</th> 
                            <th>Start Time</th> 
                            <th>Finish Time</th>
                            <th>Patient DOB</th>
                            <th>Surburb</th>
                            <th>Chaperone's Name</th>
                            <th>Doctor's Name</th>
                            <th>Branch Name</th>
                            <th>Shift Id</th>
                            <th>Medicare Voucher</th>
                            <th>Consult Note</th>
                        </tr> 
                    </thead> 
                
                    @foreach($chaperoneReport as $key=>$shift)
                    <tbody> 
                 
                     <?php $key++;
                     
                     $str = "https://myhomedoctors-inchikakoroma.c9users.io/portal/public";
                     
                    
                    if($shift->voucher_url == null){
                        $string = "/public/";
                        $label = "No image";
                        $disable1 = "disabled";
                    }else{
                        $string = $shift->voucher_url;                      
                        $label = "Voucher";
                        $disable1 = "";
                    }
                    
                    if($shift->note_url == null){                        
                        $string2 = "/public/";
                        $label2 = "No image";
                        $disable2 = "disabled";
                    }else{                        
                        $string2 = $shift->note_url;
                        $label2 = "Consult Note";
                        $disable2 = "";
                    }

                    
                    
                    list($url,$querystring) = explode('/public', $string, 2);
                    list($url2, $querystring2) = explode('/public', $string2, 2);
                     
                     
                     
                     ?>
 
                        <tr> 
                            <th scope="row">{{{ $key }}}</th>
                            <td>{{{ $shift->date }}}</td> 
                            <td>{{{ $shift->patientName }}}</td> 
                            <td>{{{ $shift->reportStart }}} </td>
                            <td>{{{ $shift->reportFinish }}}</td>
                            <td>{{{ $shift->dob }}}</td>
                            <td>{{{ $shift->surburb }}}</td>
                            <td>{{{ $shift->firstName }}} {{{ $shift->lastName }}}</td>
                            <td>{{{ $shift->doctorName }}}</td>
                            <td>{{{ $shift->branchName }}}</td>
                            <td>{{{ $shift->currentID }}}</td>
                            <td>
                             <a href="{{{$str}}}{{{$querystring}}}" data-lightbox="voucher" class="btn btn-info {{{$disable1}}}" data-title="Name:{{{$shift->patientName}}} - DOB:{{{ $shift->dob }}}">{{{$label}}}</a>
                            </td>
                            <td>
                             <a href="{{{$str}}}{{{$querystring2}}}" data-lightbox="notes" class="btn btn-info {{{$disable2}}}" data-title="Name:{{{$shift->patientName}}} - DOB:{{{ $shift->dob }}}">{{{$label2}}}</a>
                            </td>
                            
                        </tr>

       
                    </tbody>
                     @endforeach
                   

                    </table>
                 
					</div>
				</div>
			</div>
			
			@endif
                        
		</div>
 
 @stop
 