@extends('admin.layout')

@section('title')
All Staff
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>All Staff</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-table"></i>  <a href="{{{ url("dashboard/admin") }}}">Admin</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Staff
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
</div> 

<!-- /.container-fluid -->
    
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
            <div class="panel-heading">All Staff</div>
                <div class="panel-body">
                       
                    <table class="table">
                        <thead> 
                        <tr> 
                            <th>#</th>
                            <th>Name</th> 
                            <th>Email</th> 
                            <th>Branch</th>
                            <th>Mobile</th>
                            <th>DOB</th>
                            <th>Type</th>
                            <th>Last Active</th>
                            <th></th>
                        </tr> 
                        </thead>
                        
                        <tbody> 
                        @foreach($staffs as $key=>$staff)
                        <?php $key++; ?>
                        <tr> 
                            <th scope="row">{{{ $key }}}</th> 
                            <td>{{{ $staff->firstName }}} {{{ $staff->lastName }}}</td> 
                            <td>{{{ $staff->email }}}</td>
                            <td>{{{ $staff->branch }}}</td>
                            <td>{{{ $staff->mobile }}}</td>
                            <td>{{{ $staff->date_of_birth }}}</td>
                            <td>{{{ $staff->type }}}</td>
                            <td>{{{ $staff->lastActive }}}</td>
                            <td>
                            <a href="{!! route("edit-staff", [$staff->id]) !!}" class="btn btn-success">Edit</a>
                            </td>
                            
                        </tr>
                        @endforeach
                        </tbody>
               
                </div>
            </div>  
        </div>
    </div>
 
 @stop
 
