@extends('admin.layout')

@section('title')
Edit Staff
@stop

@section('content')
 
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <small>New Staff</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                   <i class="fa fa-dashboard"></i>  <a href="{{{ url("dashboard") }}}">Dashboard</a>
                </li>
                <li class="active">
                   <i class="fa fa-table"></i>  <a href="{{{ url("dashboard/admin") }}}">Admin</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Edit
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
</div> 

<!-- /.container-fluid -->
    
        <div class="row">
       
        <div class="col-sm-12">
    
              {!! Form::open(array('url' => ('user/new'))) !!}

              <div class="form-group">
                 {!! Form::label('firstName', 'First Name: ') !!}
                 <input type="text" class="form-control" name="firstName" value="{!! $staff->firstName !!}">
                <p style="color:red;">
                    {!! $errors->first('firstName') !!}
                </p>
                 
              </div>
              
              <div class="form-group">
                 {!! Form::label('lastName', 'Last Name: ') !!}
                 <input type="text" class="form-control" name="lastName" value="{!! $staff->lastName !!}">
                <p style="color:red;">
                    {!! $errors->first('lastName') !!}
                </p>
                 
              </div>
              
              
              <div class="form-group">
                
                <label>Gender</label>
                
                <label class="radio-inline">
                <input name="gender" id="gender" value="Male" checked="" type="radio">Male
                </label>
                                
                <label class="radio-inline">
                <input name="gender" id="gender" value="Female" checked="" type="radio">Female
                </label>
                
              </div>              
                            
              <div class="form-group">
                 {!! Form::label('address', 'Address: ') !!}
                 <textarea class="form-control" name="address" value="{!! $staff->address !!}"> </textarea>
                <p style="color:red;">
                    {!! $errors->first('address') !!}
                </p>
                 
              </div>

              <div class="form-group">
                 {!! Form::label('email', 'Email: ') !!}
                 <input type="email" class="form-control" name="email" value="{!! $staff->email !!}">
                 <p style="color:red;">
                     {!! $errors->first('email') !!}
                </p>
                
              </div>
              
              <div class="form-group">
                 {!! Form::label('date_of_birth', 'Date Of Birth: ') !!}
                 <input type="text" id="datepicker" class="form-control" name="date_of_birth" value="{!! $staff->date_of_birth !!}">
                 <p style="color:red;">
                    {!! $errors->first('date_of_birth') !!}
                </p>
                 
              </div>
              
              <div class="form-group">
                {!! Form::label('branchID', 'Branch: ') !!}
                <select name="branchID">
                    @foreach($branch as $myBranch)
                    <option value="{{{$myBranch->id}}}">{{{$myBranch->name}}}</option>
                    @endforeach
                </select>
              </div>
              
             
              <div class="form-group">
                {!! Form::label('password', 'Password: ') !!}
                {!! Form::password('password', array('class' => 'form-control')) !!}
                <p style="color:red;">
                {!! $errors->first('password') !!}
                </p>
              </div>

              <div class="form-group">
                {!! Form::label('password', 'Confirm Password: ') !!}
                {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                <p style="color:red;">
                {!! $errors->first('password') !!}
                </p>
              </div>

              <div class="form-group">
                {!! Form::label('type', 'Type: ') !!}
                {!! Form::select('type', [
                   'Staff' => 'Staff',
                   'Doctor' => 'Doctor',
                   'Chaperone' => 'Chaperone',
                    'Admin' => 'Admin']
                ) !!}
              </div>
              
              
              
              {!! Form::submit('Update', array('class' => 'btn btn-success')) !!}
              {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default']) !!}
             
              {!! Form::close() !!}
              
        </div>
         
    </div>
    
    
 
 @stop
 