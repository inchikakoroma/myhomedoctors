<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chaperone_Report extends Model
{
    protected $table = 'chaperone_report';
    
    public function user()
    {
        return $this->belongsTo('App\Chaperone_Shift');
    }
    
}
