<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branch';
    
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    
   
}
