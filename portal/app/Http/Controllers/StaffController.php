<?php

namespace App\Http\Controllers;
use App\User;
use App\Branch;
use View;
use DateTime;
use App\Chaperone_Shift;
use App\Chaperone_Report;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
    /**
        *** Display All Chaperone Shifts ***
     */
    public function shift()
    {
        
        $chaperoneShift = DB::select("Select users.firstName as firstName, users .lastName as lastName,
                                        chaperone_shift.id AS currentID, users.id AS userID, branch.name AS branchName,
                                         chaperone_shift.doctorName AS doctorName,
                                         DATE_FORMAT(chaperone_shift.shiftDate, '%d %M %Y') AS date,
                                        DATE_FORMAT(chaperone_shift.startTime, '%H:%i %p') AS chapStart,
                                        DATE_FORMAT(chaperone_shift.finishTime, '%H:%i %p') AS chapFinish,
                                         DATE_FORMAT(chaperone_shift.docStartTime, '%H:%i %p') AS docStart,
                                          DATE_FORMAT(chaperone_shift.docFinishTime, '%H:%i %p') AS docFinish from chaperone_shift
                                        inner join branch on chaperone_shift.branchID = branch.id
                                        inner join users on chaperone_shift.userID = users.id ORDER BY date DESC");
        
        return view('staff.staff-chaperone-shift', ['chaperoneShift' => $chaperoneShift]);
       
    }
    
     /**
        *** Display All Chaperone report ***
     */
    public function report()
    {
       
        $chaperoneReport = DB::select("SELECT chaperone_report.id AS reportID, chaperone_report.patientName as patientName,
                                        users.firstName as firstName, users.lastName as lastName,
                                        chaperone_shift.id AS currentID, users.id AS usersID, branch.name AS branchName,
                                        chaperone_shift.doctorName AS doctorName,
                                        chaperone_report.surburb AS surburb, chaperone_report.chaperoneShiftID AS myID,
                                        chaperone_report.medicareVoucher AS voucher,
                                        chaperone_report.consultNote AS consultNote,
                                        chaperone_report.medicareVoucher_url AS voucher_url, chaperone_report.consultNote_url AS note_url,
                                        DATE_FORMAT(chaperone_shift.shiftDate, '%d %M %Y') AS date,
                                        DATE_FORMAT(chaperone_report.startTime, '%H:%i %p') AS reportStart,
                                        DATE_FORMAT(chaperone_report.patientDOB, '%d %M %Y') AS dob,
                                        DATE_FORMAT(chaperone_report.finishTime, '%H:%i %p') AS reportFinish FROM chaperone_report 
                                        inner join chaperone_shift on chaperone_report.chaperoneShiftID = chaperone_shift.id
                                        inner join branch on chaperone_shift.branchID = branch.id
                                        inner join users on chaperone_shift.userID = users.id ORDER BY date DESC");
                                        
                                        
        //return $chaperoneReport;
        
        return view('staff.staff-chaperone-report', ['chaperoneReport' => $chaperoneReport]);
                                        
    }

    /*
    
    
     */
    public function create()
    {
        //
    }
    
     /*
        ***  ***
    */
    
    public function store(Request $request)
    {
        //
    }

    /*
        *** Display Chaperone Shift with reports ***
    */
    
    public function show($id)
    {
    
    $myID = $id;
    
    
        $chaperoneShift1 = DB::table('chaperone_shift')
                                        ->join('branch', 'chaperone_shift.branchID', '=', 'branch.id')
                                        ->join('users', 'chaperone_shift.userID', '=', 'users.id')
                                        ->select(DB::raw("users.firstName as firstName, users.lastName as lastName,
                                        chaperone_shift.id AS currentID, users.id AS usersID, branch.name AS branchName,
                                        chaperone_shift.rego AS rego, chaperone_shift.startKilo AS startKilo,
                                        chaperone_shift.totalKilo AS totalKilo, chaperone_shift.endKilo AS endKilo,
                                        chaperone_shift.breakTotalHour AS breakTotal, chaperone_shift.refueled AS refueled,
                                        chaperone_shift.refueledDuringShift AS refueledDuringShift, chaperone_shift.petrolCost AS cost,
                                        chaperone_shift.feedback AS feedback, chaperone_shift.doctorName AS docName, chaperone_shift.receipt_url AS receipt_url,
                                        DATE_FORMAT(chaperone_shift.shiftDate, '%d-%m-%y') AS date,
                                        DATE_FORMAT(chaperone_shift.startTime, '%H:%i %p') AS chapStart,
                                        DATE_FORMAT(chaperone_shift.finishTime, '%H:%i %p') AS chapFinish,
                                        DATE_FORMAT(chaperone_shift.breakStartTime, '%H:%i %p') AS breakStart,
                                        DATE_FORMAT(chaperone_shift.breakFinishTime, '%H:%i %p') AS breakFinish,
                                        DATE_FORMAT(chaperone_shift.docStartTime, '%H:%i %p') AS docStart,
                                        DATE_FORMAT(chaperone_shift.docFinishTime, '%H:%i %p') AS docFinish"))
                                        ->where('chaperone_shift.id', '=', $myID)->get();
                               
        $chaperoneReport = DB::select("SELECT chaperone_report.id AS reportID, chaperone_report.patientName as patientName,
                                        chaperone_report.surburb AS surburb, chaperone_report.medicareVoucher AS voucher,
                                        chaperone_report.chaperoneShiftID AS myID, chaperone_report.consultNote AS consultNote,
                                        chaperone_report.medicareVoucher_url AS voucher_url, chaperone_report.consultNote_url AS note_url,
                                        
                                        DATE_FORMAT(chaperone_shift.shiftDate, '%d %M %Y') AS date,
                                        DATE_FORMAT(chaperone_report.startTime, '%H:%i %p') AS reportStart,
                                        DATE_FORMAT(chaperone_report.patientDOB, '%d %M %Y') AS dob,
                                        DATE_FORMAT(chaperone_report.finishTime, '%H:%i %p') AS reportFinish FROM chaperone_report 
                                        inner join chaperone_shift on chaperone_report.chaperoneShiftID = chaperone_shift.id
                                        where chaperone_report.chaperoneShiftID = '$id'");
             /*  
               
               $myCode = 0;                         
                                        
            function calculate_age($birthday){
                $today = new DateTime();
                $diff = $today->diff(new DateTime($birthday));
              
   
                if ($diff->y)
                {
                    if($diff->y > 65 || $diff->y < 16){
                         global $myCode;
                         $myCode = 10992;
                        echo $myCode;
                        
                    }
                   
                }
                
    
            }; 
            
        
        
            
       
     
       */
    
    
    return View::make('staff.staff-chaperone-shift-details', compact('chaperoneShift1'), compact('chaperoneReport'));
   
    }

    /**
         *** Display Chaperone Shift by branches ***
     */
    public function branchShift($id, $name)
    {
        
        $chaperoneShift = DB::select("Select users.firstName as firstName, users .lastName as lastName,
                                        chaperone_shift.id AS currentID, users.id AS userID, branch.name AS branchName,
                                        chaperone_shift.doctorName AS doctorName,
                                        DATE_FORMAT(chaperone_shift.shiftDate, '%d %M %Y') AS date,
                                        DATE_FORMAT(chaperone_shift.startTime, '%H:%i %p') AS chapStart,
                                        DATE_FORMAT(chaperone_shift.finishTime, '%H:%i %p') AS chapFinish,
                                        DATE_FORMAT(chaperone_shift.docStartTime, '%H:%i %p') AS docStart,
                                        DATE_FORMAT(chaperone_shift.docFinishTime, '%H:%i %p') AS docFinish from chaperone_shift
                                        inner join branch on chaperone_shift.branchID = branch.id
                                        inner join users on chaperone_shift.userID = users.id
                                        WHERE branch.id = '$id' ORDER BY date DESC");
        
        
     
        
        return view('staff.staff-chaperone-shift-branch', ['chaperoneShift' => $chaperoneShift], ['name' => $name]);
    }

    /**
        *** Display All Chaperone report by branches ***
     */
    public function branchReport($id, $name)
    {
        
         $chaperoneReport = DB::select("SELECT chaperone_report.id AS reportID, chaperone_report.patientName as patientName,
         								chaperone_report.consultNote_url AS note_url, chaperone_report.medicareVoucher_url AS voucher_url,
                                        users.firstName as firstName, users.lastName as lastName,
                                        chaperone_shift.id AS currentID, users.id AS usersID, branch.name AS branchName,
                                        chaperone_shift.doctorName AS doctorName,
                                        chaperone_report.surburb AS surburb, chaperone_report.chaperoneShiftID AS myID,
                                        DATE_FORMAT(chaperone_shift.shiftDate, '%d %M %Y') AS date,
                                        DATE_FORMAT(chaperone_report.startTime, '%H:%i %p') AS reportStart,
                                        DATE_FORMAT(chaperone_report.patientDOB, '%d %M %Y') AS dob,
                                        DATE_FORMAT(chaperone_report.finishTime, '%H:%i %p') AS reportFinish FROM chaperone_report 
                                        inner join chaperone_shift on chaperone_report.chaperoneShiftID = chaperone_shift.id
                                        inner join branch on chaperone_shift.branchID = branch.id
                                        inner join users on chaperone_shift.userID = users.id 
                                        WHERE branch.id = '$id' ORDER BY date DESC");
        
        
        
        return view('staff.staff-chaperone-report-branch', ['chaperoneReport' => $chaperoneReport], ['name' => $name]);
    }
    
    
    public function consultNotes(){
        
        //$test = DB::connection('mysql2')->select("select * from users");
        $test = "Test connection";
        
       // return $consultNote;
        
        return $test;
        
        //return view('staff.staff-consult-notes', ['consultNote' => $consultNote]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
