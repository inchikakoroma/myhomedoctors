<?php

namespace App\Http\Controllers;

use App\User;
use App\Branch;
use View;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
     /**
        *** Display All Staff ***
     */
    public function index()
    {
     
        $staffs = DB::select("Select users.firstName as firstName, users .lastName as lastName,
                              users.email as email, users.id AS id, users.mobile as mobile,
                              users.type as type, users.id AS id, users.mobile as mobile,
                              branch.name as branch,
                                DATE_FORMAT(users.date_of_birth, '%d %M %Y') AS date_of_birth,
                                DATE_FORMAT(users.lastActive, '%d %M %Y') AS lastActive from users
                                inner join branch on users.branchID = branch.id");
        
        
      //  $staffs = DB::table('users')->get();
        
        
        return view('admin.all-staff', ['staffs' => $staffs]);
    }
    
    /**
        *** Display All Staff ***
     */
    public function editStaff($id)
    {
        $branch = DB::table('branch')->select(DB::raw("branch.id, branch.name"))->orderBy('name', 'ASC')->get();
       
       return view('admin.edit-staff', ['staff' => User::findOrFail($id), 'branch' => $branch]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
