<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Image;
use Input;
use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Branch;
use App\Chaperone_Shift;
use App\Chaperone_Report;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class ChaperoneController extends Controller
{
    
     protected function validator(array $data)
   {

       return Validator::make($data, [
            'startTime' => 'required|max:255',
             'date' => 'required',
            'doctorName' => 'required|max:255',
            'startKilo'  => 'required|digits_between:1,2000000',
        ]);
   }
    
    /*
        *** Display Index Page ***
     */
    public function index()
    {
        
        // get the login chaperone
        $chaperoneShift =  Chaperone_Shift::user();
       return view('chaperone.chaperone')->with(compact('chaperoneShift'));
    }

    /*
      *** Create a new Chaperone Report  ***
     */
    public function createReport(Request $request)
    {
         $chaperoneReport = new Chaperone_Report;
      
         
         $validator = Validator::make($request->all(),[
            
            'patientName' => 'required',
            'patientDOB' => 'required',
            'surburb' => 'required',
            'startTime' => 'required',
            'finishTime' => 'required',
            //'medicareVocuher' => 'required',
            //'consultNote' => 'required',
            
        ]);
         
         if($validator->passes()){
             
              // get all the inputs
                $input = $request->all();
                // assign values
                $chaperoneReport->chaperoneShiftID = $input['userID'];
                $chaperoneReport->patientName = $input['patientName'];
                $chaperoneReport->patientDOB = $input['patientDOB'];
                $chaperoneReport->surburb = $input['surburb'];
    
                $chaperoneReport->startTime = $input['startTime'];
                $chaperoneReport->finishTime = $input['finishTime'];
                
                // format date
                $chaperoneReport->patientDOB = date('Y-m-d', strtotime($chaperoneReport->patientDOB));
              
                
                if ($request->hasFile('medicareVoucher')) {
                 
                    // get the file
                    $image = $request->file('medicareVoucher');

                    $string = str_replace(' ', '', $chaperoneReport->patientName);
                    
                    //$filename  = $string . $chaperoneReport->chaperoneShiftID . '.' .$image->getClientOriginalExtension();
                    
                   $filename  = $string . $chaperoneReport->chaperoneShiftID . '.jpg';
                    // save the new image to public/images
                  
                    
                    $url = public_path('images/medicareVoucher/' . $filename);
                    
                    $chaperoneReport->medicareVoucher_url = $url;
                    
                    
                    $path = Image::make($image->getRealPath())->save($url);
                   
                   
                    $chaperoneReport->medicareVoucher = $path; 
                    
                    $path->destroy();
                    
                    
             
                   
                }
                
                $chaperoneReport->save();
           
                return redirect()->route('dashboard-chaperone-shift', [$chaperoneReport->chaperoneShiftID]);
                
                
         }else{
             
             return redirect()->back()->withInput()->withErrors($validator);
         }
       
    }

    /*
        *** Create A New Chaperone Shift ***
    */
     
    public function createShift(Request $request)
    {
        
        $chaperoneShift = new Chaperone_Shift;
        
        $validator = $this->validator($request->all());
        
            if($validator->passes()){
                
                // get all the inputs
                $input = $request->all();

                // assign values
                $chaperoneShift->userID = $input['userID'];
                $chaperoneShift->shiftDate = $input['date'];
                $chaperoneShift->startTime = $input['startTime'];
                $chaperoneShift->rego = $input['rego'];
                $chaperoneShift->otherRego = $input['otherRego'];
                $chaperoneShift->branchID = $input['branchID'];
                $chaperoneShift->doctorName = $input['doctorName'];
                $chaperoneShift->startKilo = $input['startKilo'];
                
                // format date
                $chaperoneShift->shiftDate = date('Y-m-d', strtotime($chaperoneShift->shiftDate));
                
            
               $chaperoneShift->save();
           
               return redirect()->route('dashboard-chaperone');
                
            }else{

            return redirect()->back()->withInput()->withErrors($validator);
    
        
        }

    }
    
    
     /*
        *** Display END CHAPERONE SHIFT PAGE ***
    */
    
     public function updateShift($id)
    {
        
        return view('chaperone.end-chaperone-shift', ['chaperoneShift' => Chaperone_Shift::findOrFail($id)]);
    }

    /*
        *** Display New Chaperone Report Page ***
     */
    public function show($id)
    {
        
          
         return view('chaperone.new-chaperone-report', ['chaperoneShift' => Chaperone_Shift::findOrFail($id)]);
    }

    /*
      *** Update Chaperone Report ***
     */
    public function updateReport(Request $request, $id)
    {
        $chaperoneReport = Chaperone_Report::findOrFail($id);
        
        $validator = Validator::make($request->all(),[
            
            'patientName' => 'required',
            'patientDOB' => 'required',
            'surburb' => 'required',
            'startTime' => 'required',
            'finishTime' => 'required',
          //  'medicareVocuher' => 'required',
           // 'consultNote' => 'required',
            
        ]);
        
        
         if($validator->passes()){
             
            $input = $request->all();
            
            $chaperoneReport->patientName = $input['patientName'];
            $chaperoneReport->patientDOB = $input['patientDOB'];
            $chaperoneReport->surburb = $input['surburb'];
            $chaperoneReport->startTime = $input['startTime'];
            $chaperoneReport->finishTime = $input['finishTime'];
            
            // format date
            $chaperoneReport->patientDOB = date('Y-m-d', strtotime($chaperoneReport->patientDOB));
                
            
             if ($request->hasFile('medicareVoucher')) {
                 
                    // get the file
                    $image = $request->file('medicareVoucher');
                    
                    // remove all spaces
                    $string = str_replace(' ', '', $chaperoneReport->patientName);
                    
                    
                    $filename  = $string . $chaperoneReport->chaperoneShiftID . '.' .$image->getClientOriginalExtension();
                    
                    
                    // save the new image to public/images
                  
                    
                    $path = public_path('images/medicareVoucher/' . $filename);
                    
                    
                    $chaperoneReport->medicareVoucher_url = $path;
                    
                   
                    $path = Image::make($image->getRealPath())->save($path);
                   
                    $chaperoneReport->medicareVoucher = $path; 
                    
                    $path->destroy();
                    
                   
                }
                
            
            $chaperoneReport->save();
           
            return redirect()->route('dashboard-chaperone-shift', [$chaperoneReport->chaperoneShiftID]);
             
         }else{

            return redirect()->back()->withErrors($validator)->withInput();

        }

    }

    /**
        *** END Chaperone Shift ***
     */
    public function update(Request $request, $id)
    {
        $chaperoneShift = Chaperone_Shift::findOrFail($id);
        
        // validate request
        $validator = Validator::make($request->all(), [
            
            'breakStartTime' => 'required',
            'breakFinishTime' => 'required',
            'breakTotalHour' => 'required',
            'docStartTime' => 'required',
            'docFinishTime' => 'required',
            'docTotalHour' => 'required',
            'endKilo' => 'required|digits_between:1,2000000',
            'totalKilo' => 'required',
            'finishTime' => 'required',
            
            
        ]);
        
        if($validator->passes()){

                // get all the inputs
                $input = $request->all();
                // assign values
                $chaperoneShift->breakStartTime = $input['breakStartTime'];
                $chaperoneShift->breakFinishTime = $input['breakFinishTime'];
                $chaperoneShift->breakTotalHour = $input['breakTotalHour'];
                $chaperoneShift->docStartTime = $input['docStartTime'];
                $chaperoneShift->docFinishTime = $input['docFinishTime'];
                $chaperoneShift->docTotalHour = $input['docTotalHour'];
                $chaperoneShift->endKilo = $input['endKilo'];
        		$chaperoneShift->totalKilo = $input['totalKilo'];
        		$chaperoneShift->refueled = $input['refueled'];
        		$chaperoneShift->refueledDuringShift = $input['refueledDuringShift'];
        		
        	
                if ($request->hasFile('receipt')) {
                 
                    // get the file
                    $image = $request->file('receipt');
                    
                    // create a file name
                    $filename  = $chaperoneShift->userID . '-' .$chaperoneShift->id . '-receipt.' .$image->getClientOriginalExtension();
                    
                    // save the new image to this folder
                    $path = public_path('images/fuelReceipts/' . $filename);
                    
                    $chaperoneShift->receipt_url = $path;
                    
                   // create a new image path
                    $path = Image::make($image->getRealPath())->save($path);
                   
                    $chaperoneShift->receipt = $path; 
                    
                    $path->destroy();
                   
                }
        
                $chaperoneShift->finishTime = $input['finishTime'];
                $chaperoneShift->petrolCost = $input['petrolCost'];
                $chaperoneShift->feedBack = $input['feedBack'];
        	
        
                $chaperoneShift->save();	
		
          
                return redirect()->route('dashboard-chaperone');
   
        }else{

            return redirect()->back()->withErrors($validator)->withInput();
    
        
        }
    
       
    }

    /*
        // new shift page
     */
    public function newShift()
    {
        
        $user =  Auth::user();
        
        $userBranch =  $user->branchID;
        
        $br = Branch::where('id', '=', $userBranch)->firstOrFail();
       
         $car = DB::select("Select branch.name AS branchName,
                                    branch.id AS myBranch, cars.rego AS rego from cars
                                    inner join branch on cars.branchID = branch.id
                                    WHERE branch.id = '$userBranch' ORDER BY branch.name ASC");
                                    
                                   
                                    
         $branch = DB::table('branch')->select(DB::raw("branch.id, branch.name"))->orderBy('name', 'ASC')->get();
                                     
     //return $car->branchName;
        
    	return view("chaperone.new-chaperone-shift")->with(compact('user','branch','car', 'br'));
    }
}
