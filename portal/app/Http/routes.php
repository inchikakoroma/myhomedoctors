<?php

/*
|--------------------------------------------------------------------------
|Home Page
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth.login');
});


/*
|--------------------------------------------------------------------------
| Authentication & Registration Routes
|--------------------------------------------------------------------------
|
| 
|
*/

// get login page
Route::get('auth/login', [
  //  'middleware' => 'auth',
    'as' => 'login', 
    'uses' => 'Auth\AuthController@getLogin'
]);

// submit login
Route::post('auth/login', 'Auth\AuthController@postLogin');

// logout
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// register
Route::get('register', function () {
    return view('auth.register');
});

// Post Register page
Route::post('user/new', 
    [
        //'middleware' => 'auth',
        'as' => 'addStaff', 
        'uses' => 'UserController@addUser', 
        function($id) {
    //
        }
    ]
);

Event::listen('auth.login', function($user) {
    $user->active = new DateTime;

    $user->save();
});

Event::listen('auth.logout', function($user) {
    $user->lastActive = $user->active;

    $user->save();
});


/*
|--------------------------------------------------------------------------
| User Dashboard Routes
|--------------------------------------------------------------------------
|
*/

// get the Login Staff index page
Route::get('dashboard', [
    'middleware' => 'auth',
    'uses' => 'HomeController@index'
]);


// show user's profile page
Route::get('staff/profile/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'profile', 
        'uses' => 'UserController@showProfile', 
        function($id) {
    //
        }
    ]
);

/*
|--------------------------------------------------------------------------
| Admin Dashboard Routes
|--------------------------------------------------------------------------
|
*/

// get the index page for staff
Route::get('dashboard/admin',[

    function(){
        $branch = DB::table('branch')->select(DB::raw("branch.id, branch.name"))->orderBy('name', 'ASC')->get();
        
        return view('admin.index', ['branch' => $branch]);
    }
    
   
]);

// Register
Route::get('dashboard/admin/register', [
    
    'middleware' => 'auth',
    'as' => 'register', 
    
   function(){
      $branch = DB::table('branch')->select(DB::raw("branch.id, branch.name"))->orderBy('name', 'ASC')->get();
      return view('admin.register', ['branch' => $branch]);
   } 
    
]);

// View all staff
Route::get('dashboard/admin/staff',[
    
    'middleware' => 'auth',
    'as' => 'all-staff', 
    'uses' => 'AdminController@index', 

    function(){
       
    }

]);

// Edit All Staff
Route::get('dashboard/admin/staff/{id}',[
    
    'middleware' => 'auth',
    'as' => 'edit-staff', 
    'uses' => 'AdminController@editStaff', 

    function($id){
       
    }

]);

/*
|--------------------------------------------------------------------------
| Staff Dashboard Routes
|--------------------------------------------------------------------------
|
*/

// get the index page for staff
Route::get('dashboard/staff',[

    function(){
        $branch = DB::table('branch')->select(DB::raw("branch.id, branch.name"))->orderBy('name', 'ASC')->get();
        
        return view('staff.index', ['branch' => $branch]);
    }
    
   
]);

// display All Chaperone shift page

Route::get('dashboard/staff/chaperone-shifts', [
    
    'middleware' => 'auth',
    'as' => 'staff-chaperone-shifts', 
    'uses' => 'StaffController@shift', 
    
   function(){
      
   } 
    
]);


// display All Chaperone Reports page

Route::get('dashboard/staff/chaperone-reports', [
    
    'middleware' => 'auth',
    'as' => 'staff-chaperone-reports', 
    'uses' => 'StaffController@report', 
    
   function(){
      
   } 
    
]);



// display the Staff Chaperone Shift by ID with Report Details Page

Route::get('dashboard/staff/chaperone-shift-{id}-details', [
    
    'middleware' => 'auth',
    'as' => 'staff-chaperone-shifts-details', 
    'uses' => 'StaffController@show', 
    
   function($id){
     
   } 
    
]);

// display the Staff Chaperone Shift by Branch ID Page

Route::get('dashboard/staff/chaperone-shift-branch{id}/{name}', [
    
    'middleware' => 'auth',
    'as' => 'staff-chaperone-branch-shifts', 
    'uses' => 'StaffController@branchShift', 
    
   function($id, $name){
     
   } 
    
]);

// display the Staff Chaperone Report by Branch ID Page

Route::get('dashboard/staff/chaperone-report-branch{id}/{name}', [
    
    'middleware' => 'auth',
    'as' => 'staff-chaperone-branch-reports', 
    'uses' => 'StaffController@branchReport', 
    
   function($id, $name){
     
   } 
    
]);

// display all Consultnotes
Route::get('dashboard/staff/consult-note', [
   
   'middleware' => 'auth',
   'as' =>  'staff-consult-notes',
   'uses' => 'StaffController@consultNotes'
    
]);




/*
|--------------------------------------------------------------------------
| Chaperone Dashboard Routes
|--------------------------------------------------------------------------
|
*/

// get the Chaperone index page (chaperone)
Route::get('dashboard/chaperone', 
    [
        'middleware' => 'auth',
        'as' => 'dashboard-chaperone', 
      
        function() {
            // get the login user
            $user =  Auth::user();
            // get login user's id
            $id =  $user->id;
            
            //$chaperoneShift = DB::select("Select *, DATE_FORMAT(startTime,'%H:%i') AS myDate from chaperone_shift where userID = :id", ['id' =>$id]);
           
           // sql select
          $chaperoneShift = DB::table('chaperone_shift')->select(DB::raw("chaperone_shift.id, 
                                    chaperone_shift.rego, chaperone_shift.otherRego, chaperone_shift.doctorName, 
                                    chaperone_shift.endKilo, branch.name AS branchName, 
                                    DATE_FORMAT(startTime,'%H:%i') AS myDate"))->where('userID', $id)->join('branch', 'chaperone_shift.branchID', '=', 'branch.id')->orderBy('id', 'ASC')->get();
          //return $chaperoneShift;
          return view('chaperone.chaperone', ['user' => $user, 'chaperoneShift' => $chaperoneShift]);
        }
    ]
);

// get the Chaperone Shift Page
Route::get('dashboard/chaperone/shift/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'dashboard-chaperone-shift', 
      
        function($id) {
            // get the login user id
            $myId =  Auth::user()->id;
            
             $chaperoneShift1 = DB::select("Select *, chaperone_shift.id AS currentID, branch.name AS branchName, DATE_FORMAT(chaperone_shift.startTime, '%H:%i') AS myDate from chaperone_shift inner join branch on chaperone_shift.branchID = branch.id where chaperone_shift.id = '$id' AND userID = '$myId'");
            
            // get the shift by its id
           $chaperoneShift = DB::select("Select *, chaperone_shift.id AS currentID, branch.name AS branchName, 
                                            chaperone_report.medicareVoucher_url AS voucher_url, 
                                            chaperone_report.consultNote_url AS note_url,
                                            DATE_FORMAT(chaperone_report.startTime, '%H:%i') AS start, 
                                            DATE_FORMAT(chaperone_report.finishTime, '%H:%i') AS finish,  
                                            DATE_FORMAT(chaperone_report.patientDOB, '%d %M %Y') AS dob from chaperone_shift 
                                            inner join branch on chaperone_shift.branchID = branch.id 
                                            inner join chaperone_report on chaperone_shift.id = chaperone_report.chaperoneShiftID 
                                            where chaperone_shift.id = '$id' AND userID = '$myId'");
      
           return view("chaperone.chaperone-shift")->with(compact('chaperoneShift', 'chaperoneShift1', 'id'));
            
        }
    ]
);


// END A Shaperone Shift
Route::get('dashboard/chaperone/shift/update/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'endShift', 
        'uses' => 'ChaperoneController@updateShift', 
        function($id) {
               // return view('chaperone.end-chaperone-shift');
            
        }
    ]
);

// EDIT A Shaperone Report
Route::get('dashboard/chaperone/report/edit/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'edit-report-view', 
       // 'uses' => 'ChaperoneController@updateReport', 
        function($id) {
            
             return view('chaperone.edit-chaperone-report', ['chaperoneReport' => App\Chaperone_Report::findOrFail($id)]);
               // return view('chaperone.end-chaperone-shift');
            
        }
    ]
);




// show chaperone new shift page
Route::get('dashboard/chaperone/shift', 
    [
        'middleware' => 'auth',
       'as' => 'chaperone-new-shift', 
       'uses' => 'ChaperoneController@newShift', 
        function() {
           
        }
    ]
);

// show chaperone new report page
Route::get('dashboard/chaperone/{id}/report/new', 
    [
        'middleware' => 'auth',
        'as' => 'new-chaperone-report',
        'uses' => 'ChaperoneController@show',
        function($id) {
           
        }
    ]
);

// Submits


// Submit new Chaperone Shift
Route::post('dashboard/chaperone/shift/submit', 
    [
        'middleware' => 'auth',
        //'as' => 'submit-shift', 
        'uses' => 'ChaperoneController@createShift', 
        function() {
    //
        }
    ]
);

// Submit End new Chaperone Shift
Route::post('dashboard/chaperone/shift/end/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'update-shift', 
        'uses' => 'ChaperoneController@update', 
        function() {
    //
        }
    ]
);

// Submit new Chaperone Report
Route::post('dashboard/chaperone/report/submit', 
    [
        'middleware' => 'auth',
        //'as' => 'submit-shift', 
        'uses' => 'ChaperoneController@createReport', 
        function() {
    //
        }
    ]
);

// Submit Edit Chaperone Report
Route::post('dashboard/chaperone/report/edit/submit/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'edit-report', 
        'uses' => 'ChaperoneController@updateReport', 
        function($id) {
    //
        }
    ]
);


/*
|--------------------------------------------------------------------------
| TEST
|--------------------------------------------------------------------------
|
*/



// test script (remove later)
Route::get('dashboard/test', [
    'middleware' => 'auth',
    'uses' => 'UserController@index'
]);

// get the Chaperone index page
Route::get('dashboard/test2/{id}', 
    [
        'middleware' => 'auth',
       //'as' => 'dashboard-chaperone', 
       // 'uses' => 'StaffController@showProfile', 
        function($id) {
            $myId =  Auth::user()->id;
            
           // $chaperoneShift = App\Chaperone_Shift::findOrFail($id)->where('userID', $myId);
            
             // $chaperoneShift = DB::table('chaperone_shift')->select(DB::raw("chaperone_shift.id, chaperone_shift.rego, chaperone_shift.doctorName, chaperone_shift.endKilo, branch.name AS branchName, DATE_FORMAT(startTime,'%H:%i') AS myDate"))->where('id' '')->join('branch', 'chaperone_shift.branchID', '=', 'branch.id')->orderBy('startTime', 'desc')->get();
          
            $chaperoneShift = DB::select("Select * from chaperone_shift where id = '$id' AND userID = '$myId'");
    
            return $chaperoneShift;
            
        }
    ]
);



Route::get('staff/test', [
    //'middleware' => 'auth',
    'as' => 'showStaff', 
    'uses' => 'HomeController@create'
]);



/*
// show user's profile page
Route::get('dashboard/chaperone/shift/{id}', 
    [
        'middleware' => 'auth',
        'as' => 'chaperone-new-shift', 
        'uses' => 'ChaperoneController@showShift', 
        function($id) {
    //
        }
    ]
);

// show chaperone new shift page
Route::get('dashboard/chaperone/shift/new', 
    [
        'middleware' => 'auth',
       'as' => 'new-shift', 
       'uses' => 'StaffController@showProfile', 
        function() {
            $user =  Auth::user();
            return view("chaperone.new-chaperone-shift")->with(compact('user'));
        }
    ]
);




*/