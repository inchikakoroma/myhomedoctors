<?php 
$obj_ambulance = new Hmgt_ambulance();
if(isset($_REQUEST['save_ambulance']))
{

	if(isset($_REQUEST['action']) && ($_REQUEST['action'] == 'insert' || $_REQUEST['action'] == 'edit'))
	{

		$result = $obj_ambulance->hmgt_add_ambulance($_POST);
		if($result)
		{
			if($_REQUEST['action'] == 'edit')
			{
				?><div id="message" class="updated below-h2"><?php
				_e("Record updated successfully",'hospital_mgt');
				?>
				</div>
			<?php }
			else 
			{?>
			<div id="message" class="updated below-h2">
			<?php 
				_e('Record inserted successfully','hospital_mgt');
			?></div><?php }
			
			
		}
	}
}
if(isset($_REQUEST['save_ambulance_request']))
{

	if(isset($_REQUEST['action']) && ($_REQUEST['action'] == 'insert' || $_REQUEST['action'] == 'edit'))
	{

		$result = $obj_ambulance->hmgt_add_ambulance_request($_POST);
		if($result)
		{
			if($_REQUEST['action'] == 'edit')
			{
				wp_redirect ( home_url() . '?dashboard=user&page=ambulance&tab=ambulance_req_list&message=2');
			}
			else 
			{
				wp_redirect ( home_url() . '?dashboard=user&page=ambulance&tab=ambulance_req_list&message=1');
			}
			
			
		}
	}
}
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete')
{
	if($_GET['page'] == 'ambulance')
	{
		$result = $obj_ambulance->delete_ambulance_req($_REQUEST['amb_req_id']);
	}
	
	if($result)
	{
			wp_redirect ( home_url() . '?dashboard=user&page=ambulance&tab=ambulance_req_list&message=3');
	}
}

$edit=0;
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
		
	$edit=1;
	$result= $obj_ambulance->get_single_ambulance_req($_REQUEST['amb_req_id']);
		
}
if(isset($_REQUEST['message']))
{
	$message =$_REQUEST['message'];
	if($message == 1)
	{?>
			<div id="message" class="updated below-h2 ">
			<p>
			<?php 
				_e('Record inserted successfully','hospital_mgt');
			?></p></div>
			<?php 
		
	}
	elseif($message == 2)
	{?><div id="message" class="updated below-h2 "><p><?php
				_e("Record updated successfully.",'hospital_mgt');
				?></p>
				</div>
			<?php 
		
	}
	elseif($message == 3) 
	{?>
	<div id="message" class="updated below-h2"><p>
	<?php 
		_e('Record deleted successfully','hospital_mgt');
	?></div></p><?php
			
	}
}	

$active_tab = isset($_GET['tab'])?$_GET['tab']:'ambulance_req_list';
?>
<script type="text/javascript">
$(document).ready(function() {
	
	$('.request_time').timepicker();
	$('.dispatch_time').timepicker();
	$('#request_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>
<div class="panel-body panel-white">
 <ul class="nav nav-tabs panel_tabs" role="tablist">
      <li class="<?php if($active_tab == 'ambulance_req_list'){?>active<?php }?>">
          <a href="?dashboard=user&page=ambulance&tab=ambulance_req_list">
             <i class="fa fa-align-justify"></i> <?php _e('Chaperone Report', 'hospital_mgt'); ?></a>
          </a>
      </li>
      <li class="<?php if($active_tab == 'add_ambulance_req'){?>active<?php }?>">
      <a href="?dashboard=user&page=ambulance&tab=add_ambulance_req">
        <i class="fa fa-plus-circle"></i> 
        <?php 
        if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit')
        	 _e('Edit Report', 'hospital_mgt'); 
        else 
        _e('New Report', 'hospital_mgt'); 
        ?></a> 
      </li>
</ul>
<script type="text/javascript">
$(document).ready(function() {
	jQuery('#ambulance_list').DataTable({
		 "order": [[ 2, "Desc" ]],
		 "aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                         
	                  {"bSortable": false}
	               ],
		language:<?php echo datatable_multi_language();?>
		});
		
	
} );
</script>
	<div class="tab-content">
	<?php if($active_tab == 'ambulance_req_list'){?>
    	 <div class="tab-pane fade active in" id="prescription">
         <?php 
		 //	$retrieve_class = get_all_data($tablename);		
		?>
		<div class="panel-body">
         <div class="table-responsive">
        <table id="ambulance_list" class="display dataTable" cellspacing="0" width="100%">
        	 <thead>
            <tr>
			<th><?php _e( 'Chaperone', 'hospital_mgt' ) ;?></th>
			   <th><?php _e( 'Patient', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Date', 'hospital_mgt' ) ;?></th>	
				<th><?php _e( 'Branch', 'hospital_mgt' ) ;?></th>	
				<th><?php _e( 'Start Time', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Finish Time', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Issues', 'hospital_mgt' ) ;?></th>
				<th><?php  _e( 'Action', 'hospital_mgt' ) ;?></th>
            </tr>
        </thead>
		<tfoot>
            <tr>
				<th><?php _e( 'Chaperone', 'hospital_mgt' ) ;?></th>
			    <th><?php _e( 'Patient', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Date', 'hospital_mgt' ) ;?></th>	
				<th><?php _e( 'Branch', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Start Time', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Finish Time', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Issues', 'hospital_mgt' ) ;?></th>
				<th><?php _e( 'Action', 'hospital_mgt' ) ;?></th>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
         
         if($obj_hospital->role == 'pharmacist')
         		
         	$ambulancereq_data=$obj_ambulance->get_ambulance_request_by_chaperone(get_current_user_id());
         else
         
				$ambulancereq_data=$obj_ambulance->get_all_ambulance_request();
		 
		 if(!empty($ambulancereq_data))
		 {
		 	foreach ($ambulancereq_data as $retrieved_data){ 
		 		$patient_data =	get_user_detail_byid($retrieved_data->patient_id);?>
            <tr>
            	
            	
            	
				<td class="ambulanceid">
					<?php 
						$created = get_user_detail_byid($retrieved_data->amb_create_by); 
               			echo $created['first_name']." ".$created['last_name'];
					?>
				</td>
                <td class="patient"><?php echo $retrieved_data->charge;?></td>
                <td class="date"><?php echo $retrieved_data->request_date;?></td>
                <td class="date"><?php echo $retrieved_data->branch;?></td>
				<td class="time"><?php echo $retrieved_data->request_time;?></td>
                <td class="dispatchtime"><?php echo $retrieved_data->dispatch_time;?></td>
                <td class="dispatchtime"><?php echo $retrieved_data->some_issue;?></td>
               	<td class="action"> 
               	<a href="?dashboard=user&page=ambulance&tab=add_ambulance_req&action=edit&amb_req_id=<?php echo $retrieved_data->amb_req_id;?>" class="btn btn-info"> <?php _e('Edit', 'hospital_mgt' ) ;?></a>
                <a href="?dashboard=user&page=ambulance&tab=ambulance_req_list&action=delete&amb_req_id=<?php echo $retrieved_data->amb_req_id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','hospital_mgt');?>');">
                <?php _e( 'Delete', 'hospital_mgt' ) ;?> </a>              
               
            </tr>
            <?php } 
			
		}?>
        </tbody>
        
        </table>
        </div>
        </div>
        
		
	</div>
	<?php }
	if($active_tab == 'add_ambulance_req'){
	?>
	
	<div class="tab-pane fade active in" id="add_req">
       <script type="text/javascript">
$(document).ready(function() {
	$('#patient_form').validationEngine();
	$('#request_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>	
       <div class="panel-body">
         <form name="patient_form" action="" method="post" class="form-horizontal" id="patient_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="amb_req_id" value="<?php if(isset($_REQUEST['amb_req_id']))echo $_REQUEST['amb_req_id'];?>"  />
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="ambulance_id"><?php _e('Car','hospital_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="ambulance_id" class="form-control validate[required] " id="ambulance_id">
					<option value=""><?php _e('Select Car','hospital_mgt');?></option>
					<?php 
					if($edit)
						$amb_id = $result->ambulance_id;
					elseif(isset($_REQUEST['ambulance_id']))
						$amb_id = $_REQUEST['ambulance_id'];
					else 	
						$amb_id = "";
						$ambulance_data=$obj_ambulance->get_all_ambulance();
					 	if(!empty($ambulance_data))
					 	{
					 		foreach ($ambulance_data as $retrieved_data)
					 		{ 
					 			echo '<option value = '.$retrieved_data->amb_id.' '.selected($amb_id,$retrieved_data->amb_id).'>'.$retrieved_data->ambulance_id.'</option>';
					 		}
					 	}						
					 ?>
				</select>
				
			</div>
		</div>
		
		<div class="form-group">
                <label class="col-sm-2 control-label" for="report_cost"><?php _e('Branch','hospital_mgt');?><span class="require-field">*</span></label>
                <div class="col-sm-8">
                <select name="branch" id="category_data" class="form-control validate[required]">
                	<option value=""><?php if($edit) echo $result->branch; else echo "Select Branch"; ?></option>
                    <option value="Armidale">Armidale</option>
                    <option value="Auburn">Auburn</option>
                    <option value="Brisbane">Brisbane</option>
                    <option value="Cessnock">Cessnock</option>
                    <option value="Gosford">Gosford</option>
                    <option value="Mackay">Mackay</option>
                    <option value="Newcastle">Newcastle</option>
                    <option value="Redcliffe">Redcliffe</option>
                    <option value="Toowoomba">Toowoomba</option>
                </select>
               </div>
          </div>
	
		<div class="form-group">
			<label class="col-sm-2 control-label" for="charges"><?php _e("Patient's Name",'hospital_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="charges" class="form-control validate[required]" type="text"  value="<?php if($edit){ echo $result->charge;}elseif(isset($_POST['charge'])) echo $_POST['charge'];?>" name="charge">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="address"><?php _e('Address','hospital_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<textarea name = "address" id="address" class="form-control validate[required]"><?php if($edit){ echo $result->address;}elseif(isset($_POST['address'])) echo $_POST['address'];?></textarea>
			</div>
		</div>
				
		<div class="form-group">
			<label class="col-sm-2 control-label" for="request_date"><?php _e('Date','hospital_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="request_date" class="form-control validate[required]" type="text"  value="<?php if($edit){ echo $result->request_date;}elseif(isset($_POST['request_date'])) echo $_POST['request_date'];?>" name="request_date">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="request_time"><?php _e('Start Time','hospital_mgt');?></label>
			<div class="col-sm-8">
				<input id="request_time" class="form-control request_time" type="text" data-show-meridian="false"  data-default-time="00:15" value="<?php if($edit){ echo $result->request_time;}elseif(isset($_POST['request_time'])) echo $_POST['request_time'];?>" name="request_time">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="dispatch_time"><?php _e('Finish Time','hospital_mgt');?></label>
			<div class="col-sm-8">
				<input id="dispatch_time" class="form-control dispatch_time" data-show-meridian="false" data-minute-step="15"type="text"  value="<?php if($edit){ echo $result->dispatch_time;}elseif(isset($_POST['dispatch_time'])) echo $_POST['dispatch_time'];?>" name="dispatch_time">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="address"><?php _e('Issues (If Any)','hospital_mgt');?></label>
			<div class="col-sm-8">
				<textarea name = "some_issue" id="address" class="form-control"><?php if($edit){ echo $result->some_issue;}elseif(isset($_POST['some_issue'])) echo $_POST['some_issue'];?></textarea>
			</div>
		</div>
		<div class="col-sm-offset-2 col-sm-8">
        	<input type="submit" value="<?php if($edit){ _e('Save','hospital_mgt'); }else{ _e('Add','hospital_mgt');}?>" name="save_ambulance_request" class="btn btn-success"/>
        </div>
        </form>
        </div>
         
	</div>
	<?php }?>
	</div>
</div>
<?php ?>